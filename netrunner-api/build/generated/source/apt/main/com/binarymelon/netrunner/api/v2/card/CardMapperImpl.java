package com.binarymelon.netrunner.api.v2.card;

import com.binarymelon.netrunner.service.Card;

import java.util.ArrayList;

import java.util.List;

import javax.annotation.Generated;

import org.springframework.stereotype.Component;

@Generated(

    value = "org.mapstruct.ap.MappingProcessor",

    date = "2017-07-29T11:37:25-0300",

    comments = "version: 1.2.0.CR1, compiler: javac, environment: Java 1.8.0_141 (Oracle Corporation)"

)

@Component

public class CardMapperImpl implements CardMapper {

    @Override

    public CardDto toDto(Card entity) {

        if ( entity == null ) {

            return null;
        }

        CardDto cardDto = new CardDto();

        cardDto.setCode( entity.getCode() );

        cardDto.setCost( entity.getCost() );

        cardDto.setDeckLimit( entity.getDeckLimit() );

        cardDto.setFactionCode( entity.getFactionCode() );

        cardDto.setFactionCost( entity.getFactionCost() );

        cardDto.setFlavor( entity.getFlavor() );

        cardDto.setIllustrator( entity.getIllustrator() );

        cardDto.setKeywords( entity.getKeywords() );

        cardDto.setPackCode( entity.getPackCode() );

        cardDto.setPosition( entity.getPosition() );

        cardDto.setQuantity( entity.getQuantity() );

        cardDto.setSideCode( entity.getSideCode() );

        cardDto.setText( entity.getText() );

        cardDto.setTitle( entity.getTitle() );

        cardDto.setTypeCode( entity.getTypeCode() );

        cardDto.setUniqueness( entity.getUniqueness() );

        return cardDto;
    }

    @Override

    public Card toEntity(CardDto dto) {

        if ( dto == null ) {

            return null;
        }

        Card card = new Card();

        card.setCode( dto.getCode() );

        card.setCost( dto.getCost() );

        card.setDeckLimit( dto.getDeckLimit() );

        card.setFactionCode( dto.getFactionCode() );

        card.setFactionCost( dto.getFactionCost() );

        card.setFlavor( dto.getFlavor() );

        card.setIllustrator( dto.getIllustrator() );

        card.setKeywords( dto.getKeywords() );

        card.setPackCode( dto.getPackCode() );

        card.setPosition( dto.getPosition() );

        card.setQuantity( dto.getQuantity() );

        card.setSideCode( dto.getSideCode() );

        card.setText( dto.getText() );

        card.setTitle( dto.getTitle() );

        card.setTypeCode( dto.getTypeCode() );

        card.setUniqueness( dto.getUniqueness() );

        return card;
    }

    @Override

    public List<CardDto> toDtos(List<Card> entities) {

        if ( entities == null ) {

            return null;
        }

        List<CardDto> list = new ArrayList<CardDto>( entities.size() );

        for ( Card card : entities ) {

            list.add( toDto( card ) );
        }

        return list;
    }

    @Override

    public List<Card> toEntities(List<CardDto> dtos) {

        if ( dtos == null ) {

            return null;
        }

        List<Card> list = new ArrayList<Card>( dtos.size() );

        for ( CardDto cardDto : dtos ) {

            list.add( toEntity( cardDto ) );
        }

        return list;
    }
}

