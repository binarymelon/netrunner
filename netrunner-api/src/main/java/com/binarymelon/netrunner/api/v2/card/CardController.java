package com.binarymelon.netrunner.api.v2.card;

import com.binarymelon.netrunner.service.Card;
import com.binarymelon.netrunner.service.CardService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by ryan on 2017-07-23
 */
@RestController("api/v2/cards")
@Api(value="netrunner", description="Cards operation")
public class CardController {
    private final CardService cardService;
    private final CardMapper cardMapper;

    @Autowired
    public CardController(CardService cardService, CardMapper cardMapper) {
        this.cardService = cardService;
        this.cardMapper = cardMapper;
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CardDto>> getList() {
        return ResponseEntity.ok(cardMapper.toDtos(cardService.getList()));
    }
}
