package com.binarymelon.netrunner.api.v2.card;

/**
 * Created by ryan on 2017-07-23
 */
public class CardDto {
    private String code;
    private Integer cost;
    private Integer deckLimit;
    private String factionCode;
    private Integer factionCost;
    private String flavor;
    private String illustrator;
    private String keywords;
    private String packCode;
    private Integer position;
    private Integer quantity;
    private String sideCode;
    private String text;
    private String title;
    private String typeCode;
    private Boolean uniqueness;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Integer getDeckLimit() {
        return deckLimit;
    }

    public void setDeckLimit(Integer deckLimit) {
        this.deckLimit = deckLimit;
    }

    public String getFactionCode() {
        return factionCode;
    }

    public void setFactionCode(String factionCode) {
        this.factionCode = factionCode;
    }

    public Integer getFactionCost() {
        return factionCost;
    }

    public void setFactionCost(Integer factionCost) {
        this.factionCost = factionCost;
    }

    public String getFlavor() {
        return flavor;
    }

    public void setFlavor(String flavor) {
        this.flavor = flavor;
    }

    public String getIllustrator() {
        return illustrator;
    }

    public void setIllustrator(String illustrator) {
        this.illustrator = illustrator;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getPackCode() {
        return packCode;
    }

    public void setPackCode(String packCode) {
        this.packCode = packCode;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getSideCode() {
        return sideCode;
    }

    public void setSideCode(String sideCode) {
        this.sideCode = sideCode;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public Boolean getUniqueness() {
        return uniqueness;
    }

    public void setUniqueness(Boolean uniqueness) {
        this.uniqueness = uniqueness;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CardDto cardDto = (CardDto) o;

        if (code != null ? !code.equals(cardDto.code) : cardDto.code != null) return false;
        if (cost != null ? !cost.equals(cardDto.cost) : cardDto.cost != null) return false;
        if (deckLimit != null ? !deckLimit.equals(cardDto.deckLimit) : cardDto.deckLimit != null) return false;
        if (factionCode != null ? !factionCode.equals(cardDto.factionCode) : cardDto.factionCode != null) return false;
        if (factionCost != null ? !factionCost.equals(cardDto.factionCost) : cardDto.factionCost != null) return false;
        if (flavor != null ? !flavor.equals(cardDto.flavor) : cardDto.flavor != null) return false;
        if (illustrator != null ? !illustrator.equals(cardDto.illustrator) : cardDto.illustrator != null) return false;
        if (keywords != null ? !keywords.equals(cardDto.keywords) : cardDto.keywords != null) return false;
        if (packCode != null ? !packCode.equals(cardDto.packCode) : cardDto.packCode != null) return false;
        if (position != null ? !position.equals(cardDto.position) : cardDto.position != null) return false;
        if (quantity != null ? !quantity.equals(cardDto.quantity) : cardDto.quantity != null) return false;
        if (sideCode != null ? !sideCode.equals(cardDto.sideCode) : cardDto.sideCode != null) return false;
        if (text != null ? !text.equals(cardDto.text) : cardDto.text != null) return false;
        if (title != null ? !title.equals(cardDto.title) : cardDto.title != null) return false;
        if (typeCode != null ? !typeCode.equals(cardDto.typeCode) : cardDto.typeCode != null) return false;
        return uniqueness != null ? uniqueness.equals(cardDto.uniqueness) : cardDto.uniqueness == null;
    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 31 * result + (cost != null ? cost.hashCode() : 0);
        result = 31 * result + (deckLimit != null ? deckLimit.hashCode() : 0);
        result = 31 * result + (factionCode != null ? factionCode.hashCode() : 0);
        result = 31 * result + (factionCost != null ? factionCost.hashCode() : 0);
        result = 31 * result + (flavor != null ? flavor.hashCode() : 0);
        result = 31 * result + (illustrator != null ? illustrator.hashCode() : 0);
        result = 31 * result + (keywords != null ? keywords.hashCode() : 0);
        result = 31 * result + (packCode != null ? packCode.hashCode() : 0);
        result = 31 * result + (position != null ? position.hashCode() : 0);
        result = 31 * result + (quantity != null ? quantity.hashCode() : 0);
        result = 31 * result + (sideCode != null ? sideCode.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (typeCode != null ? typeCode.hashCode() : 0);
        result = 31 * result + (uniqueness != null ? uniqueness.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CardDto{" +
                "code='" + code + '\'' +
                ", cost=" + cost +
                ", deckLimit=" + deckLimit +
                ", factionCode='" + factionCode + '\'' +
                ", factionCost=" + factionCost +
                ", flavor='" + flavor + '\'' +
                ", illustrator='" + illustrator + '\'' +
                ", keywords='" + keywords + '\'' +
                ", packCode='" + packCode + '\'' +
                ", position=" + position +
                ", quantity=" + quantity +
                ", sideCode='" + sideCode + '\'' +
                ", text='" + text + '\'' +
                ", title='" + title + '\'' +
                ", typeCode='" + typeCode + '\'' +
                ", uniqueness=" + uniqueness +
                '}';
    }
}
