package com.binarymelon.netrunner.api.v2.card;

import com.binarymelon.netrunner.service.Card;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Created by ryan on 2017-07-29
 */
@Mapper(componentModel = "spring")
public interface CardMapper {
    CardDto toDto(Card entity);
    Card toEntity(CardDto dto);

    @IterableMapping(qualifiedByName = "toDto")
    List<CardDto> toDtos(List<Card> entities);

    @IterableMapping(qualifiedByName = "toEntity")
    List<Card> toEntities(List<CardDto> dtos);
}
