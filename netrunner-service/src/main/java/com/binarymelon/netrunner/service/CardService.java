package com.binarymelon.netrunner.service;

import java.util.List;

/**
 * Created by ryan on 2017-07-29
 */
public interface CardService {
    List<Card> getList();
}
