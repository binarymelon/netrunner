package com.binarymelon.netrunner.service;

import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * Created by ryan on 2017-07-29
 */
@Service
public class CardServiceImpl implements CardService {
    @Override
    public List<Card> getList() {
        return mockCardList();
    }

    private List<Card> mockCardList() {
        final Card card = new Card();
        card.setCode("01003");
        card.setCost(2);
        card.setDeckLimit(3);
        card.setFactionCode("anarch");
        card.setFactionCost(2);
        card.setFlavor("You ever set something on fire just to watch it burn?");
        card.setIllustrator("Anna Ignatieva");
        card.setKeywords("Run - Sabotage");
        card.setPackCode("code");
        card.setPosition(3);
        card.setQuantity(3);
        card.setSideCode("runner");
        card.setText("Make a run on HQ or R&D. You may trash, at no cost, any cards you access (even if the cards cannot normally be trashed).");
        card.setTitle("Demolition Run");
        card.setTypeCode("event");
        card.setUniqueness(false);
        return Collections.singletonList(card);
    }
}
